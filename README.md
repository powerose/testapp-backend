# TestApp-Server API Documentation

## Installation
Please check out [INSTALL.md] for detailed information.
## Usage
https://testapp-server.hostname.tld/?command&option1=value&option2=foo

TestApp API will always return a JSON encoded value.

## Known options
*Note: Some features are outdated. Please check out the source code or wait till we'll update the documentation.*
```
## command  options=defaultValue            # explinaition
# Help options
help                                        # Displays a little help message
# User management
login       mail, pass, [stay=false]        # Logs in the given user for 90 minutes / forever
logout                                      # Logs out the current user destroying all sessions and cookies
loginState                                  # Checks if the user is logged in or not
register    mail, pass, school, name        # Creates a new user
updateUser  user, [pass|mail|school|name]   # User as id/mail, updates the user information for given user
userInfo    [user]                          # Fetches user details about given or current user
# Exercises
addExercise name content topics correct answers # Adds exercise for given information. correct to be array containing the value of all correct answers.
updateExercise exercise [name|content|topics|correct|aw1|...|aw5]   # Updates the information for given exercise. Same values like addExercise
deleteExercise exercise                     # Deletes the given exercise
fetchExercises [exercise|topics number]     # Fetching given exercise or random exercises by topics and number
uploadImage exercise                        # Uploads image for given exercise. Expects image in $_FILES["file"]
removeImage exercise                        # Removes image for given exercise.
# JoinTests
joinTest id                                 # Fetching exercises for given (previusly activated) JoinID
addJoin exercises name                      # Creating new JoinTest (disactivated by default)
deleteJoin id                               # Removes given JoinTest
listJoin [showAll=true]                     # Lists current user's JoinTests or all
updateJoin id [exercises|name]              # Updates given JoinTest with new exercises/name
activateJoinTest id time                    # Creates a new instance of given JoinTest. Valid for $time minutes
# Test Usage
saveTest answers privacy="anonymous" [user|join]    # Saves written test. answers as assoc array {(int)exe:string(answer)}. user is defaultly curren user. join may contain the id of a JoinTest
testScore id                                # Returns score of given test as integer
listTests [user]                            # List tests of current or given user
userScore                                   # Shows score of current user.
userSubjectScore subject                        # Average score of given subject (string) of current user.
topicScore topics [user]                    # Calculates the average score of given topic for all teacher's students or given student (last one, allowed for students as well)
classScore class [topics]                   # Displays average score of tests associated with a class. Special topics may be specified. For admins only
joinScore join                              # Shows score of all tests associated with given JoinID. Admins only
# Class Management
addClass name                               # Adds class with given name. Admins only
listClasses                                 # Returns a list of own classes. Admins only
deleteClass class                           # Deletes class with given id
updateClass class [name|topics|students]    # Updates given class' information. Just avalible for class' admin
inClasses [user]                            # Displays list of classes, of which given/current user is part of
leaveClass class                            # Allowes current user to leave a given class
# General
listTopics                                  # Returns a list of all topics and their subjects
addTopic name subject                       # Creates new topic for given subject. An unused topic will be deleted automatically after being unused for two weeks. For admins only
listSchools                                 # Displays a list of all Schools
```
chaw’: nIqHom lo’, jIQochbe’ quv jIlij jIjatlhmo’ tlhIngan Hol.