<?php
$arguments = $_GET;
if (isset($arguments["nodel"])) {
    $del = false;
} else {
    $del = true;
}
if (isset($arguments["host"]) && isset($arguments["user"]) && isset($arguments["pass"]) && isset($arguments["table"])) {
    //Creating DATABASE
    $creade_db = "CREATE DATABASE " . $arguments["table"];
    $db = new mysqli($arguments["host"], $arguments["user"], $arguments["pass"]);
    $db->query($creade_db);
    //Creating and filling tables
    $db = new mysqli($arguments["host"], $arguments["user"], $arguments["pass"], $arguments["table"]);
    $install_sql = file_get_contents("install.sql");
    $db->multi_query($install_sql) or die("Error creating tables.");
    //Deleting install.sql
    if ($del) {
        unlink("install.sql");
    }

    $db_connection = "<?php
class db extends mysqli{
    public function __construct() {
        \$this->connect(\"" . $arguments["host"] . "\", \"" . $arguments["user"] . "\", \"" . $arguments["pass"] . "\", \"" . $arguments["table"] . "\");
    }
}
?>";
    $file = fopen("db.class.php", "w+");
    $success = fwrite($file, $db_connection);
    if ($success == false) {
        die(false);
    }

    return true;
}
