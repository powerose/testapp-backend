<?php
require_once("/usr/share/php/libphp-phpmailer/PHPMailerAutoload.php");
function mailer () {
    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
    try {
        //Server settings
        //$mail->SMTPDebug = 2;                               // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'mail.example.com';                     // Specify SMTP server
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'testapp@example.com';              // SMTP username
        $mail->Password = 'secret_password';                  // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom('testapp@example.com', 'TestApp Team');

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML

        return $mail;
    } catch (Exception $e) {
            $GLOBALS["errors"][]="Error connecting mail server.";
    }
}
?>