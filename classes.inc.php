<?php
//make PHP showing errors
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/
ini_set('session.use_cookies', '0');
require_once "db.class.php";
class testapp
{
    public $user_info = false;

    public function __construct($sid = null)
    {
        //Creating MySQL connection
        $this->db = new db();

        $this->clean_db();

        session_start();
        //Starting session
        if (!is_null($sid)) {
            $GLOBALS["errors"][] = "Changed id!";
            session_id($sid);
        } else {
            $sid = session_id();
        }
        $this->session_id = $sid;

        if ($this->check_login()) {
            $this->user_info = $this->get_user(null);
            //Updating timestamp in db
            if (isset($_COOKIE["id"])) {
                //Stay logged in for 12 years
                $timestamp = time() + 60 * 60 * 24 * 365 * 12;
            } else {
                //Stay logged in for 1.5h
                $timestamp = time() + 60 * 90;
            }
            $this->db->query("UPDATE `sessions` SET `timestamp`=$timestamp+`offset` WHERE id='" . $this->session_id . "'");
        }
    }

    ###User Management###
    public function login(string $mail, string $password, bool $stay = true)
    {
        //Destroying previous sessions
        $this->logout();
        //escaping mail from injections
        $mail = $this->db->real_escape_string($mail);
        //Fetching 'all' users with given mail
        $result = $this->db->query("SELECT * FROM user WHERE mail='$mail'");
        //Checking wether # of result==1
        if ($result->num_rows == 1) {
            $row = $result->fetch_object();
            //Verifiying the passwords
            if (password_verify($password, $row->pass)) {
                //There's nothing to do...
            } elseif (md5(sha1($password)) == $row->pass) {
                // Must update to new standard if still stored as md5(sha1("Password"))
                $id = $row->id;
                $pass = password_hash($password, PASSWORD_DEFAULT);
                $this->db->query("UPDATE user SET pass='$pass' WHERE id=$id");
            } else {
                //If the passwords don't match...
                return false;
            }
            //Saving user information into session
            $_SESSION["accesslevel"] = $row->accesslevel;
            $this->session_id = $this->session_id;
            $_SESSION["mail"] = $row->mail;
            $_SESSION["user_id"] = $row->id;
            //Saving HTTP_USER_AGENT for DB storage
            $agent = $this->db->real_escape_string($_SERVER["HTTP_USER_AGENT"]);

            $offset = 60 * 90;
            //if marked to stay logged in
            if ($stay == true) {
                //Stay logged in for 12 years
                $offset =  60 * 60 * 24 * 365 * 12;
            }
            $timestamp = time() + $offset;

            //Saving session into DB
            $this->db->query("INSERT INTO `sessions` (id, user, agent, `timestamp`, `offset`) VALUES('" . $this->db->real_escape_string($this->session_id) . "', " . $_SESSION["user_id"] . ", '$agent', $timestamp, $offset)")
            or die("Error saving session.");
            return true;
        }
        //If there's no user with given mail
        return false;
    }
    public function restore_session(string $session_id)
    {
        session_id($session_id);
        $this->session_id = $session_id;
        return $this->check_login();
    }
    public function logout()
    {
        //Checking if session is initialized
        //if($this->check_login()) {
        if (isset($this->session_id)) {
            $this->db->query("DELETE FROM `sessions` WHERE id='" . $this->db->real_escape_string($this->session_id) . "'");
        }

        session_unset();
        session_destroy();
        return true;
        //}
        //return true;
    }
    public function check_login()
    {
        if (isset($this->session_id) == false) {
            return false;
        }

        $id = $this->session_id;
        $result = $this->db->query("SELECT * FROM `sessions` WHERE id='$id'");
        return ($result->num_rows >= 1);
    }
    public function create_user_account(string $mail, string $pass, string $name, int $school, int $accesslevel = 0, $voucher = null)
    {
        //Checking voucher
        if ($accesslevel > 0) {
            $voucher = $this->db->real_escape_string($voucher);
            $timestamp = time();
            $vouchers = $this->sql_to_array("SELECT * FROM `vouchers` WHERE `code`='$voucher' AND `school`=$school `expiry`>$timestamp");
            if (count($vouchers) < 1) {
                $GLOBALS["errors"][] = "INVALID_VOUCHER";
                return (false);
            }
            $this->db->query("DELETE FROM `vouchers` WHERE `code`='$voucher'");
        }

        $name = $this->db->real_escape_string(ucwords($name));
        $pass = $this->db->real_escape_string($pass);
        $mail = $this->db->real_escape_string(strtolower($mail));
        $school = $this->db->real_escape_string($school);
        $pass = $this->db->real_escape_string(password_hash($pass, PASSWORD_DEFAULT));

        //Write new user into DB
        $sql = "INSERT INTO user (`name`, `pass`, `mail`, `school`, `accesslevel`) VALUES ('$name', '$pass', '$mail', $school, $accesslevel);";
        $result = $this->db->query($sql);

        //$this->mailer($mail, "TestApp registration", "Wellcome to TestApp! We provide you a cross-platform way of smart leaning. You just registered with the mail id $mail and the name $name. If this registration was not performed by you, please contact us, so we'll unlink your account.");

        return $result;
    }
    public function get_user($user = null)
    {
        //expecting null/mail/id
        $user = $this->db->real_escape_string($user);
        //If for current user, fetching user_id
        if ($user == null) {
            if ($this->check_login() == false) {
                $GLOBALS["errors"][] = "NO_SESSION";
                return false;
            }
            $session_id = $this->db->real_escape_string($this->session_id);
            $result = $this->db->query("SELECT * FROM `sessions` WHERE id='$session_id'");
            if ($result->num_rows == 1) {
                $user = $result->fetch_object()->user;
            } else {
                return false;
            }
        }

        $result = $this->db->query("SELECT * FROM user WHERE mail='$user' OR id='$user'");
        if ($result->num_rows == 1) {
            $user = $result->fetch_object();
            unset($user->pass);
            return $user;
        } else {
            return false;
        }
    }
    public function update_user($user, array $update)
    {
        //expecting mail/id of user and an array containing new information
        //checking wether id/mail
        if (is_numeric($user)) {
            //checking if admin
            if (isset($this->user_info->id) == false or $this->admin_check() == false) {
                return false;
            }
        } else {
            if ($this->check_login() == false) {
                $GLOBALS["errors"][] = "NO_SESSION";
                return false;
            }
            $user = $this->user_info->id;
        }
        //Preparing and escaping the values
        if (isset($update["pass"])) {
            $update["pass"] = $this->db->real_escape_string(password_hash($update["pass"], PASSWORD_DEFAULT));
        }

        if (isset($update["name"])) {
            $update["name"] = $this->db->real_escape_string(ucwords($update["name"]));
        }

        if (isset($update["mail"])) {
            $update["mail"] = $this->db->real_escape_string(strtolower($update["mail"]));
        }

        if (isset($update["school"])) {
            $update["school"] = $this->db->real_escape_string($update["school"]);
        }

        if (count($update) == 0) {
            return false;
        }

        $sql = "UPDATE user SET ";
        foreach ($update as $key => $value) {
            $sql .= "$key='$value', ";
        }
        $sql = substr($sql, 0, -2);
        $sql .= " WHERE id=$user";
        $this->db->query($sql) or die("Error in MySQL query.");
        return true;
    }
    public function password_reset($mail)
    {
        //updating password with random pattern for given mail and submitting new password to given mail.

        $sql_mail = $this->db->real_escape_string($mail);
        $new_pass = bin2hex(openssl_random_pseudo_bytes(4));
        $password_hash = $this->db->real_escape_string(password_hash($new_pass, PASSWORD_DEFAULT));

        $sql = "UPDATE `user` SET `pass`='$password_hash' WHERE `mail`='$sql_mail';";

        $this->db->query($sql) or die("Error in MySQL query.");

        $this->mailer($mail, "TestApp password reset", "As requested, we performed a password reset of your TestApp account.<br/>Your new password will be: <b>$new_pass</b>");

        return true;
    }
    public function admin_check()
    {
        if ($this->check_login() == false or $this->user_info->accesslevel < 0) {
            return false;
        }
        return true;
    }
    public function flame()
    {
        if ($this->check_login() == false) {
            $GLOBALS["errors"][] = "NO_SESSION";
            return false;
        }
        $sql = "SELECT * FROM `tests` WHERE `user`=" . $this->user_info->id;
        $tests = $this->sql_to_array($sql);
        $times = [];
        foreach ($tests as $test) {
            $times[] = $test->timestamp;
        }
        rsort($times);
        $flame_count = 1;
        $current_time = time();
        while ($times[0] + (24 * 3600 * $flame_count) > $current_time) {
            //echo($times[0]-$current_time);
            //Deleting other timestamps in same day
            while ($times[0] + (24 * 3600 * $flame_count) > $current_time) {
                array_shift($times);
            }
            $flame_count++;
        }
        return ($flame_count);
    }
    ###End User Management###

    ###Exercises###
    public function add_exercise(string $name, string $exercise, array $topics, array $correct, string $awtype, array $answers, $tags=null, $explanation=null)
    {
        //$awtype will be generated automatically
        //Correct to contain the exact value
        //Answers are passed as array

        if ($this->admin_check() == false) {
            return false;
        }

        //Sorting $correct (needed for later cmpare if user answer is right)
        if ($awtype == "checkbox" || $awtype == "inputMultiple") {
            sort($correct);
        }

        //Escaping valiables
        $name = $this->db->real_escape_string(htmlspecialchars($name));
        $exercise = $this->db->real_escape_string(htmlspecialchars($exercise));

        for ($i = 0; $i < count($correct); $i++) {
            $correct[$i] = htmlspecialchars($correct[$i]);
        }
        $correct = $this->db->real_escape_string(json_encode($correct));
        $awtype = $this->db->real_escape_string($awtype);
        $topics = $this->db->real_escape_string(json_encode($topics));
        if($tags!=null) {
            $tags=$this->db->real_escape_string($tags);
        }
        if($explanation!=null) {
            $explanation=$this->db->real_escape_string($explanation);
        }
        //Checking if the exercise exists already
        $result = $this->db->query("SELECT * FROM `exercises` WHERE correct='$correct' AND content='$exercise' AND aw1='$answers[0]'");
        if ($result->num_rows == 1) {
            return (true);
        }

        //Preparing SQL statement
        $sql = "INSERT INTO exercises (`name`, content, awtype, topic, correct, aw1, aw2, aw3, aw4, aw5";

        if($explanation!=null) {
            $sql.=", explanation";
        }
        if($tags!=null) {
            $sql.=", tags";
        }

        $sql.=") VALUES('$name', '$exercise', '$awtype', '$topics', '$correct', ";
        //Creating empty answers-array for the case that answers is null (in case of input)
        if ($answers == null) {
            $answers = [];
        }
        //Preparing & escaping answers
        for ($i = 0; $i < 5; $i++) {
            if (isset($answers[$i])) {
                $sql .= "'" . $this->db->real_escape_string(htmlspecialchars($answers[$i])) . "', ";
            } else {
                $sql .= "'', ";
            }
        }
        $sql = substr($sql, 0, -2);
        if($explanation!=null) {
            $sql.=", '$explanation'";
        }
        if($tags!=null) {
            $sql.=", '$tags'";
        }

        $sql .= ")";
        $this->db->query($sql) or die("Error in MySQL query.");
        return true;
    }
    public function update_exercise(int $id, array $update)
    {
        //Modifiable: name, content, topics, aw1-5, correct
        //Not modifiable yet: img, awtype

        //checking if admin
        if (isset($this->user_info->id) == false or $this->admin_check() == false) {
            return false;
        }
        if (isset($update["name"])) {
            $update["name"] = $this->db->real_escape_string(htmlspecialchars($update["name"]));
        }

        if (isset($update["content"])) {
            $update["content"] = $this->db->real_escape_string(htmlspecialchars($update["content"]));
        }

        if (isset($update["topic"])) {
            $update["topic"] = $this->db->real_escape_string(json_encode($update["topic"]));
        }

        if (isset($update["explanation"])) {
            $update["explanation"] = $this->db->real_escape_string(htmlspecialchars($update["explanation"]));
        }

        if (isset($update["tags"])) {
            $update["tags"] = $this->db->real_escape_string(json_encode($update["tags"]));
        }

        for ($i = 0; $i < count($update["correct"]); $i++) {
            $update["correct"][$i] = htmlspecialchars($update["correct"][$i]);
        }
        if (isset($update["correct"])) {
            $update["correct"] = $this->db->real_escape_string(json_encode($update["correct"]));
        }

        if (isset($update["awtype"])) {
            $update["awtype"] = $this->db->real_escape_string($update["awtype"]);
        }

        //Writing all answers into sql-cell name array
        if (isset($update["answers"])) {
            $update["aw1"] = $this->db->real_escape_string(htmlspecialchars($update["answers"][0]));
        }

        if (isset($update["answers"][1])) {
            $update["aw2"] = $this->db->real_escape_string(htmlspecialchars($update["answers"][1]));
        }

        if (isset($update["answers"][2])) {
            $update["aw3"] = $this->db->real_escape_string(htmlspecialchars($update["answers"][2]));
        }

        if (isset($update["answers"][3])) {
            $update["aw4"] = $this->db->real_escape_string(htmlspecialchars($update["answers"][3]));
        }

        if (isset($update["answers"][4])) {
            $update["aw5"] = $this->db->real_escape_string(htmlspecialchars($update["answers"][4]));
        }

        //Removing answers Array from $update
        unset($update["answers"]);
        if (count($update) == 0) {
            return false;
        }

        //Generating SQL statement
        $sql = "UPDATE exercises SET ";
        foreach ($update as $key => $value) {
            $sql .= "$key='$value', ";
        }
        $sql = substr($sql, 0, -2);
        $sql .= " WHERE id=$id";
        $this->db->query($sql) or die("Error in MySQL query.");
        return true;
    }
    public function exercise_activation(int $exercise, int $activtion)
    {
        //checking if admin
        if (isset($this->user_info->id) == false or $this->admin_check() == false) {
            return false;
        }
        $this->db->query("UPDATE exercises SET `deactivate`=$activtion WHERE id=$exercise;");
        return (true);
    }
    public function upload_image(int $exercise)
    {
        //checking if admin
        if (isset($this->user_info->id) == false or $this->admin_check() == false) {
            return false;
        }
        if ($_FILES["file"]["size"] != 0) {
            $img = $this->db->real_escape_string("data:" . $_FILES["file"]["type"] . ";base64," . base64_encode(file_get_contents($_FILES["file"]["tmp_name"])));
            $this->db->query("UPDATE exercises SET `img`='$img' WHERE id=$exercise");
            return (true);
        }
        return (false);
    }
    public function remove_image(int $exercise)
    {
        //checking if admin
        if (isset($this->user_info->id) == false or $this->admin_check() == false) {
            return false;
        }
        $this->db->query("UPDATE exercises SET `img`='' WHERE id=$exercise");
        return (true);
    }
    public function delete_exercise(int $exercise)
    {
        //checking if admin
        if (isset($this->user_info->id) == false or $this->admin_check() == false) {
            return false;
        }
        return $this->db->query("DELETE FROM exercises WHERE id=" . $this->db->real_escape_string($exercise)) or die("Error in MySQL query.");
    }
    public function fetch_exercises($id = null, $topics = null, int $number = null, bool $show_img = false, $query = null)
    {
        $sql = "SELECT * FROM exercises";
        //If fetching speciffic exercise
        if ($id != null) {
            if (is_array($id)) {
                $sql .= " WHERE";
                foreach ($id as $curr_id) {
                    $sql .= " id=$curr_id OR";
                }
                $sql = substr($sql, 0, -3);
            } else {
                $sql .= " WHERE id=$id";
            }
        }
        //If topics specified
        if ($topics != null) {
            $sql .= " WHERE id IN (" . implode(", ", $this->exercise_ids_by_topic_ids($topics)) . " )";
            if ($this->admin_check() == false) {
                $sql .= " AND `deactivate`=0";
            }
        }
        $sql .= " ORDER BY id DESC";
        $return = $this->sql_to_array($sql);

        //Removing correct answer from result
        if ($id == null) {
            if (is_array($return) && empty($return) == false) {
                foreach ($return as $exercise) {
                    unset($exercise->correct);
                }
            }
        }
        //Removing images from result if unwanted
        if ($show_img != true && $id == null) {
            if (is_array($return) && empty($return) == false) {
                foreach ($return as $exercise) {
                    unset($exercise->img);
                }
            }
        }
        //Searching for query
        if($query!=null) {
            if (is_array($return) && empty($return) == false) {
                $initial_length = count($return);

                $word_list = str_split(strtolower($query), 4);
                if(strlen($word_list[count($word_list)-1])<4) {
                    $word_list[count($word_list)-1]=substr($word_list[count($word_list)-2].$word_list[count($word_list)-1],strlen($word_list[count($word_list)-1]));
                }

                for ($i = 0; $i<$initial_length; $i++) {
                    $exercise = $return[$i];
                    $handle=$exercise->name." ".$exercise->content." ".$exercise->aw1." ".$exercise->aw2." ".$exercise->aw3." ".$exercise->aw4." ".$exercise->aw5." ".$exercise->tags." ".$exercise->explaination;
                    $found_word = false;

                    foreach ( $word_list as $word) {
                        if(strpos(strtolower($handle),$word) !== false) {
                            $found_word = true;
                        }
                    }
                    if($found_word == false) {
                        unset($return[$i]);
                    }
                }

                usort($return, function ($a, $b) use ($query) {
                    $handle_a=$a->name." ".$a->content." ".$a->aw1." ".$a->aw2." ".$a->aw3." ".$a->aw4." ".$a->aw5." ".$a->tags." ".$a->explaination;
                    $handle_b=$a->name." ".$b->content." ".$b->aw1." ".$b->aw2." ".$b->aw3." ".$b->aw4." ".$b->aw5." ".$b->tags." ".$b->explaination;

                    similar_text($query, strtolower($handle_a), $percentA);
                    similar_text($query, strtolower($handle_b), $percentB);
                
                    return $percentA === $percentB ? 0 : ($percentA > $percentB ? -1 : 1);
                });
            }
        }
        //Selecting $number of exercises
        if ($number != null) {
            if ($number > sizeof($return)) {$number = sizeof($return);}
            $return_indexes = array_rand($return, $number);
            //For just $number=1 to create an array anyway...
            if ($number == 1) {
                $return_indexes = [$return_indexes];
            }

            $return_temp = [];
            foreach ($return_indexes as $index) {
                $return_temp[] = $return[$index];
            }
            $return = $return_temp;
        }
        return $return;
    }
    ###End Exercises###

    ###JoinTest Management###
    public function join_test(int $id)
    {
        if ($this->check_login() == false) {
            $GLOBALS["errors"][] = "NO_SESSION";
            return false;
        }
        $timestamp = time();
        $sql = "SELECT * FROM `join_instances` WHERE id=$id AND `expiry`>$timestamp";
        $join_id = $this->sql_to_array($sql);
        if (empty($join_id)) {
            return (false);
        }

        //Checking if current user is in given class
        $in_class = false;
        $classes = $this->in_classes();
        foreach ($classes as $current_class) {
            if ($join_id[0]->class == $current_class->id) {
                $in_class = true;
            }
        }
        if ($in_class == false) {
            return (false);
        }

        $writing_time = $join_id[0]->writing_time;
        $join_id = $join_id[0]->join_test;
        $sql = "SELECT * FROM `join` WHERE id=$join_id";
        //Fetching exercises ids
        $exercises = $this->sql_to_array($sql);
        $exercises = json_decode($exercises[0]->exercises);
        $exercises = $this->fetch_exercises($exercises, null, null, true);
        $exercises["joinTestTime"] = $writing_time;
        return ($exercises);
    }
    public function add_join_test(array $exercises, string $name)
    {
        //checking if admin
        if ($this->admin_check() == false) {
            return false;
        }
        //Computing $topics
        $exe_temp = $this->fetch_exercises($exercises);
        $topics = [];
        foreach ($exe_temp as $exe) {
            foreach (json_decode($exe->topic) as $curr_topic) {
                $topics[] = $curr_topic;
            }
        }
        sort($topics);
        $name = $this->db->real_escape_string($name);
        $exercises = $this->db->real_escape_string(json_encode($exercises));
        $topics = $this->db->real_escape_string(json_encode($topics));
        $user = $this->user_info->id;
        $this->db->query("INSERT INTO `join` (`name`, exercises, topics, `admin`, `timestamp`) VALUES('$name', '$exercises', '$topics', $user, " . time() . ")") or die("Error in MySQL query.");
        return (true);
    }
    public function delete_join_test(int $id)
    {
        if ($this->admin_check() == false) {
            return (false);
        }
        $this->db->query("DELETE FROM `join` WHERE id=$id") or die("Error in MySQL query.");
        return (true);
    }
    public function list_join_tests(bool $show_all = true)
    {
        if ($this->admin_check() == false) {
            return (false);
        }

        $sql = "SELECT * FROM `join`";
        if ($show_all == false) {
            $sql .= " WHERE `admin`=" . $this->user_info->id;
        }
        $return = $this->sql_to_array($sql);
        if (is_array($return) == false) {
            return (false);
        }

        $exercise_sets = [];

        foreach($return as $exercise_set) {
            $topics = [];
            $subjects = [];
            $topic_ids = [];
            $exercises = $this->fetch_exercises(json_decode($exercise_set->exercises, true));
            foreach($exercises as $exercise) {
                foreach(json_decode($exercise->topic, true) as $topic) {
                    $loaded_topic = $this->list_topics($topic)[0];
                    if(in_array($loaded_topic["id"], $topic_ids)==false) {
                        $topic_ids[] = $loaded_topic["id"];
                        $topics[] = $loaded_topic;
                        if(in_array($loaded_topic["subject"], $subjects)==false) {
                            $subjects[] = $loaded_topic["subject"];
                        }
                    }
                }
            }
            $current_exercise_set=[];
            $current_exercise_set["id"] = $exercise_set->id;
            $current_exercise_set["name"] = $exercise_set->name;
            $current_exercise_set["exercises"] = $exercise_set->exercises;
            $current_exercise_set["admin"] = $exercise_set->admin;
            $current_exercise_set["topics"] = $topic_ids; // for compatibility
            $current_exercise_set["fullTopics"] = $topics;
            $current_exercise_set["timestamp"] = $exercise_set->timestamp;
            $current_exercise_set["subjects"] = $subjects;
            $exercise_sets[]=$current_exercise_set;
        }

        return ($exercise_sets);
    }
    public function update_join_test(int $id, array $exercises = null, string $name = null)
    {
        if (is_null($exercises) && is_null($name)) {
            return false;
        }

        if ($this->admin_check() == false) {
            return false;
        }

        $sql = "UPDATE `join` SET ";
        if (is_null($exercises) == false) {
            sort($exercises);
            $exercises = $this->db->real_escape_string(json_encode($exercises));
            $sql .= "exercises='$exercises'";
        }
        if (is_null($name) == false) {
            $name = $this->db->real_escape_string($name);
            if (is_null($exercises) == false) {
                $sql .= ", ";
            }

            $sql .= "`name`='$name'";
        }
        $sql .= " WHERE id=$id";
        $this->db->query($sql) or die("Error in MySQL query.");
        return (true);

    }
    public function save_test($user = null, array $answers, string $privacy = "anonymous", int $join = null, int $assignment = null)
    {
        if ($this->check_login() == false) {
            $GLOBALS["errors"][] = "NO_SESSION";
            return false;
        }
        //Fetching current user #id
        if (is_null($user) || is_int($user) == false) {
            $user = $this->user_info->id;
        } else {
            //If user #id given, check for admin rightsexercises
            //(Non-admins are not allowed to save tests for other users)
            if ($this->admin_check() != true) {
                return false;
            }

        }
        //Escaping arguments
        $privacy = $this->db->real_escape_string($privacy);

        //Sorting two dimensional array of $answers
        $exercises = array_keys($answers);
        //Fetching exercise info from db
        $exercises_info_tmp = $this->sql_to_array("SELECT * FROM `exercises` WHERE `id` in (" . implode(", ", $exercises) . ")");
        $exercise_info = [];
        //creating id=>awtype Array
        for ($i = 0; $i < count($exercises_info_tmp); $i++) {
            $offset = $exercises_info_tmp[$i]->id;
            $exercise_info[$offset] = $exercises_info_tmp[$i]->awtype;
        }
        unset($exercises_info_tmp);

        $answers_new = [];
        //Going threw $exercises for the right order
        for ($i = 0; $i < count($exercises); $i++) {
            $offset = $exercises[$i];
            if (!is_array($answers[$offset])) {
                $answer_temp = json_decode($answers[$offset]);
            } else {
                $answer_temp = $answers[$offset];
            }
            //Only sorting of needed
            if ($exercise_info[$offset] != "fillInText" && $exercise_info[$offset] != "fillInRegEx") {
                sort($answer_temp);
            }

            $answers_new[$offset] = $answer_temp;
        }
        //Saving test frame data
        $time = time();
        $exercises_string = $this->db->real_escape_string(json_encode($exercises));
        $test_id = 0;
        if (is_null($assignment)) {
            if (is_null($join)) {
                $sql = "INSERT INTO tests (exercises, privacy, user, `timestamp`) VALUES('$exercises_string', '$privacy', $user, $time)";
            } else {
                $sql = "INSERT INTO tests (exercises, privacy, user, `timestamp`, join_id) VALUES('$exercises_string', '$privacy', $user, $time, $join)";
            }
            $this->db->query($sql) or die("Error in MySQL query.");
            $test_id = $this->db->insert_id;
        } else {
            //Checking if assignment was already submitted
            $submission = $this->sql_to_array("SELECT * FROM `tests` WHERE `user`=$user AND `assignment`=$assignment");
            if($submission==false||count($submission)==0) {
                //If not, creating submission
                $sql = "INSERT INTO tests (exercises, privacy, user, `timestamp`, `assignment`) VALUES('$exercises_string', '$privacy', $user, $time, $assignment)";
                $this->db->query($sql) or die("Error in MySQL query.");
                $test_id = $this->db->insert_id;
            } else {
                //Otherwise, updating
                $test_id = $submission[0]->id;
                $this->db->query("UPDATE `tests` SET `timestamp`=$time WHERE id=$test_id");
                //Removing old submission
                $this->db->query("DELETE FROM `answers` WHERE `exercise` IN (".implode(', ', $exercises).")");
            }
            
        }
        //Saving every answer into answers table and fetching ids
        $answer_sql = "";
        foreach ($answers_new as $exercise => $answer) {
            if (is_array($answer) || is_object($answer)) { // Creating an array of the (single?) answer if it isn't yet
                $answer = $this->db->real_escape_string(json_encode($answer));
            } else {
                $answer = $this->db->real_escape_string(json_encode([$answer]));
            }
            $answer_sql .= "INSERT INTO answers (test, user, exercise, answer, `timestamp`) VALUES($test_id, $user, $exercise, '$answer', $time);\n";
        }
        $this->db->multi_query($answer_sql);
        unset($answer_sql);
        //Beginning real saving
        return ($test_id);
    }
    public function activate_join_test(int $id, int $writing_time, int $expiry, int $class)
    {
        if ($this->admin_check() == false) {
            return (false);
        }

        $test = $this->sql_to_array("SELECT * FROM `join` WHERE id=$id");
        if (is_array($test) == false) {
            return (false);
        }

        $admin = $this->user_info->id;
        $expiry = time() + ($expiry * 60);
        $writing_time = $writing_time * 60;
        $sql = "INSERT INTO `join_instances` (join_test, `admin`, `expiry`, `writing_time`, `class`) VALUES($id, $admin, $expiry, $writing_time, $class)";
        $this->db->query($sql) or die("Error in MySQL query.");
        $instance_id = $this->db->insert_id;
        return ([
            "id" => $instance_id,
            "expiry" => $expiry,
            "writingTime" => $writing_time,
        ]);
    }
    public function add_assignment(int $id, int $expiry, String $name, String $describtion, int $class)
    {
        if ($this->admin_check() == false) {
            return (false);
        }

        $test = $this->sql_to_array("SELECT * FROM `join` WHERE id=$id");
        if (is_array($test) == false) {
            return (false);
        }

        $exercises = $test[0]->exercises;
        $admin = $this->user_info->id;
        $name = $this->db->real_escape_string($name);
        $describtion = $this->db->real_escape_string($describtion);
        $sql = "INSERT INTO `assignments` (`join_test`, `name`, `admin`, `expiry`, `describtion`, `exercises`, `class`) VALUES($id, '$name', $admin, $expiry, '$describtion', '$exercises', $class)";
        $this->db->query($sql) or die("Error in MySQL query.");
        $instance_id = $this->db->insert_id;
        return ([
            "id" => $instance_id,
            "expiry" => $expiry,
            "name" => $name,
            "describtion" => $describtion,
        ]);
    }
    ###End JoinTest Management###

    ###General Test Score###
    private function verify_answer(int $answer)
    {
        //Expects int as answer id
        $answer = $this->sql_to_array("SELECT * FROM answers WHERE id=$answer");
        if (is_array($answer) == false) {
            return (false);
        }

        //Getting user's answer
        $given_answer = $answer[0]->answer;
        //Fetching exercise of answer
        $exercise = $this->fetch_exercises($answer[0]->exercise);
        if ($exercise == false) {
            return ("del");
        }
//Exercise doesn't exist anymore
        //Checking if answers match
        if ($exercise[0]->awtype == "regex") {
            //Matching regex to JSON-parsed given answer
            $matches = (preg_match("/" . $exercise[0]->aw1 . "/iu", json_decode($given_answer)[0], $mts) === 1); //regex stored in aw1; /i for case insensitive /u for unicode
        } else if ($exercise[0]->awtype == "fillInRegEx" || $exercise[0]->awtype == "multipleRegEx") {
            for ($i = 1; $i <= 5; $i++) {
                //
            }
            $matches = (preg_match("/" . $exercise[0]->aw1 . "/iu", json_decode($given_answer)[0], $mts) === 1); //regex stored in aw1; /i for case insensitive /u for unicode
        } else {
            //For most other awtypes we just need to compare strings
            $matches = ($exercise[0]->correct == $given_answer);
        }
        //Returning compare
        $return = [
            "exercise" => $exercise[0]->id,
            "answer" => $given_answer,
            "correct" => $exercise[0]->correct,
            "question" => $exercise[0]->content,
            "match" => $matches,
        ];
        return ($return);
    }
    public function admin_user_topics(int $user)
    {
        //Returns a list of overlapping topics of admin and user
        if ($this->admin_check() == false) {
            return false;
        }

        //Verifying for admin, that topics are allowed
        $admin = $this->user_info->id;
        //Saving admin's classes with given student into array
        $fetched_topics = $this->sql_to_array("SELECT * FROM classes WHERE admin=$admin AND students RLIKE '(\"|,|^|\\[)$user(\"|,|$|\\])'");
        if (is_array($fetched_topics) == false) {
            return (false);
        }

        $allowed_topics = [];
        //Saving all fetched topics into array
        foreach ($fetched_topics as $fetched_topic) {
            $allowed_topics = array_merge($allowed_topics, json_decode($fetched_topic->topics));
        }
        return ($allowed_topics);
    }
    public function test_score(int $id, bool $is_join_test = false)
    {
        //Calculating the userof test
        $user = $this->sql_to_array("SELECT user FROM tests WHERE id=$id")[0]->user;
        //Checking login
        if ($this->check_login() && (is_null($user) || $user == $this->user_info->id)) {
            //$user=$this->user_info->id;
            $user = $this->user_info->id;
            $answers_sql = "";
        } elseif ($this->admin_check()) {
            if ($is_join_test) {
                //If it's a JoinTest, only fetching tests which are
                $answers_sql = ""; //" AND join_id!=null";
            } else {
                //Checking if test is stored private
                if ($this->sql_to_array("SELECT privacy FROM tests WHERE id=$id")[0]->privacy == "private") {
                    return (false);
                }

                //Fetching allowed exercises for later sql statement
                $allowed_topics = $this->admin_user_topics($user);
                $allowed_exercises = $this->fetch_exercises(null, $allowed_topics);
                //Getting all exercises ids
                $ids = $this->object_column($allowed_exercises, "id");
                $answers_sql = " AND exercise IN (" . implode(", ", $ids) . ")";

            }
        } else {
            $GLOBALS["errors"][] = "NO_SESSION";
            return false;
        }
        //Verifying user and fetching all answers for given test
        $answers = $this->sql_to_array("SELECT * FROM answers WHERE user=$user AND test=$id" . $answers_sql);
        if (is_array($answers) == false) {
            return (false);
        }

        $test_score = 0;
        $test_answers = [];
        foreach ($answers as $answer) {
            //If answer of given id correct, add 1 to score
            $answer_matching = $this->verify_answer($answer->id);
            if ($answer_matching == "del") { //Exercise of anser doesn't exist anymore
                unset($answers[array_search($answer, $answers)]); //Unsetting answer from answers array
            } elseif ($answer_matching["match"] == true) {
                //Writing exercise,answer and correct into $test_answers array
                $test_answers[] = $answer_matching;
                //Increasing score
                $test_score++;
            } else {
                //Writing exercise,answer and correct into $test_answers array
                $test_answers[] = $answer_matching;
            }
        }
        if (count($answers) == 0) {
            return ("del"); //If 0, all exercises have been deleted
        }
        //Calculating average
        $test_score /= count($answers);
        return ([
            "score" => $test_score,
            "answers" => $test_answers,
        ]);
    }
    public function list_tests($user = null)
    {
        //Getting right user and checking for admin
        if ($this->check_login() && (is_null($user) || $user == $this->user_info->id)) {
            $user = $this->user_info->id;
            //For explaination, see admin block
            $privacy_sql = "";
        } elseif ($this->admin_check()) {
            if (is_null($user)) {
                $this->user_info->id;
            }

            //Just fetching allowed tests
            $privacy_sql = " AND privacy='public'";
        } else {
            $GLOBALS["errors"][] = "NO_SESSION";
            return false;
        }
        $user_tests = $this->sql_to_array("SELECT * FROM tests WHERE user=$user $privacy_sql AND `assignment` IS NULL");
        if (is_array($user_tests) == false) {
            return (false);
        }

        return ($user_tests);
    }
    public function topic_score($user = null, array $topics)
    {
        //Getting right user and checking for admin
        if ($this->check_login() && (is_null($user) || $user == $this->user_info->id)) {
            $user = $this->user_info->id;
            //See an admin block...
            $privacy_sql = "";
        } elseif ($this->admin_check()) {
            //Selecting all overlapping admin and given arrays
            $topics = array_intersect($topics, $this->admin_user_topics($user));

            //Checkin for all tests with right privacy level as well
            $allowed_tests = $this->sql_to_array("SELECT id FROM tests WHERE user=$user AND privacy IN ('public', 'anonymous') AND `join_id` IS NULL AND `assignment` IS NULL");
            if (is_array($allowed_tests) == false) {
                return (false);
            }

            $ids = [];
            //Fetching all ids in array
            foreach ($allowed_tests as $test) {
                $ids[] = $test->id;
            }
            //Generating sql string
            $privacy_sql = implode(", ", $ids);
            $privacy_sql = " AND test IN ($privacy_sql)";
        } else {
            $GLOBALS["errors"][] = "NO_SESSION";
            return false;
        }
        //Generating sql for all given answers (including privacy check)
        $sql = implode(", ", $this->exercise_ids_by_topic_ids($topics));
        $sql = "SELECT * FROM answers WHERE user=$user AND exercise IN ($sql)$privacy_sql";
        $user_aswers = $this->sql_to_array($sql);
        if (is_array($user_aswers) == false) {
            return (false);
        }

        //Beginning real score calcs
        $score = 0;
        foreach ($user_aswers as $answer) {
            //If answer of given id correct, add 1 to score
            $answer_matching = $this->verify_answer($answer->id)["match"];
            if ($answer_matching == true && is_string($answer_matching) == false) {
                $score++;
            } elseif ($answer_matching == "del") { //Exercise of anser doesn't exist anymore
                unset($user_aswers[array_search($answer, $user_aswers)]); //Unsetting answer from answers array
            }
        }
        //Calculating average
        $score /= count($user_aswers);
        return ($score);
        ######
    }
    public function join_score(int $id)
    {
        //Checking for admin rights
        if ($this->admin_check() == false) {
            return false;
        }

        //Comparing test admin with current user
        /*$admin=$this->sql_to_array("SELECT `admin` FROM `join` WHERE id=$id")[0]->admin;
        if($admin!=$this->user_info->id) return false;*/
        //Fetching all tests with given join_id
        $sql = "SELECT * FROM tests WHERE join_id=$id";
        $fetched_tests = $this->sql_to_array($sql);
        if (is_array($fetched_tests) == false) {
            return (false);
        }

        $tests = [];
        $total_score = 0;
        foreach ($fetched_tests as $test) {
            $test_score = $this->test_score($test->id, true)["score"];
            $test_user = $this->get_user($test->user);
            $test_part = [
                "id" => $test->id,
                "score" => $test_score,
                "user" => $test_user,
            ];
            $tests[] = $test_part;
            $total_score += $test_score;
        }
        //Computing average test score
        $total_score /= count($fetched_tests);
        $return = [
            "score" => $total_score,
            "tests" => $tests,
        ];
        return ($return);
    }
    public function class_score(int $class, $topics = null)
    {
        if ($this->admin_check() == false) {
            return (false);
        }

        //Fetching classes with given id and current user as admin
        $class = $this->sql_to_array("SELECT * FROM classes WHERE id=$class AND `admin`=" . $this->user_info->id);
        if (is_array($class) == false) {
            return (false);
        }

        $students = json_decode($class[0]->students);
        //Checking if special topics are given
        if (is_array($topics)) {
            //Just taking overlapping topics
            $topics = array_intersect(json_decode($class[0]->topics), $topics);
        } else {
            $topics = json_decode($class[0]->topics);
        }
        //Student-wise
        $score_by_student = [];
        $class_score = 0;
        //Taking average of topic score of all students
        foreach ($students as $student) {
            $current_student_score = $this->topic_score($student, $topics);
            $class_score += $current_student_score;
            $score_by_student[] = [
                "id" => $student,
                "info" => $this->get_user($student),
                "score" => $current_student_score,
            ];
        }
        $class_score /= count($students);

        //Topic-wise
        $topics_by_student = [];
        foreach ($topics as $topic) {
            $average_topic_score = 0;
            $topic_score_by_student = [];
            //Taking average of topic score of all students
            foreach ($students as $student) {
                $current_student_score = $this->topic_score($student, [$topic]);
                $average_topic_score += $current_student_score;
                $topic_score_by_student[] = [
                    "id" => $student,
                    "info" => $this->get_user($student),
                    "score" => $current_student_score,
                ];
                unset($current_student_score);
            }
            $average_topic_score /= count($students);
            $topics_by_student[] = [
                "topic" => $this->list_topics($topic)[0],
                "score" => $average_topic_score,
                "students" => $topic_score_by_student,
            ];
            unset($average_topic_score);
            unset($topic_score_by_student);
        }

        $return = [
            "score" => $class_score,
            "students" => $score_by_student,
            "topics" => $topics_by_student,
        ];
        return ($return);
    }
    public function user_score()
    {
        $user_tests = $this->list_tests();
        if (is_array($user_tests) == false) {
            return (false);
        }

        $score = 0;
        foreach ($user_tests as $test_id) {
            $test_score = $this->test_score($test_id->id);
            if (is_array($test_score)) {
                $score += $test_score["score"];
            } elseif ($test_score == "del") {
                unset($user_tests[array_search($test_id, $user_tests)]);
            }
        }
        if (count($user_tests) == 0) {
            return (false);
        }

        $score /= count($user_tests);
        return ($score);
    }
    public function user_list_assignments()
    {
        $classes = $this->in_classes();
        if($classes == false) return([]);

        $class_ids = [];
        $class_names = [];
        foreach ($classes as $class) {
            $class_names[$class->id] = $class->name;
            $class_ids[] = $class->id;
        }

        $return = [];

        $sql = "SELECT * FROM assignments WHERE `class` IN (".implode(", ", $class_ids).") ORDER BY `expiry` ASC";
        $assignments = $this->sql_to_array($sql);

        $user_id =  $this->user_info->id;
        foreach ($assignments as $assignment) {
            $submission_data = 0;
            $submission_id = false;
            $sql="SELECT * FROM tests WHERE user=$user_id AND `assignment`=$assignment->id";
            $submission = $this->sql_to_array($sql);
            $answers=[];
            if($submission!=false&&count($submission)!=0) {
                if($assignment->expiry < time()) $submission_id = $submission[0]->id;
                $submission_test = $this->test_score($submission[0]->id);
                $answers_raw = $this->sql_to_array("SELECT * FROM `answers` WHERE `test`=".$submission[0]->id." AND `user`=$user_id");
                foreach ($answers_raw as $answer) {
                    $answers[] = [
                        "exercise" => $answer->exercise,
                        "answer" => json_decode($answer->answer)
                    ];
                }
                $submission_data = (count(array_keys($submission_test["answers"])) / count(json_decode($assignment->exercises)));
            }
            $return[] = [
                "id" => $assignment->id,
                "expiry" => $assignment->expiry,
                "name" => $assignment->name,
                "describtion" => $assignment->describtion,
                "class" => $class_names[$assignment->class],
                "exercises" => $assignment->exercises,
                "completed" => $submission_data,
                "answers" => $answers,
                "testId" => $submission_id
            ];
        }

        return ($return);
    }
    public function user_subject_score(string $subject)
    {
        $subject = $this->db->real_escape_string($subject);
        $topics = $this->sql_to_array("SELECT `id` from `topics` WHERE `subject`='$subject'");
        $topics = $this->object_column($topics, "id");
        $score = $this->topic_score(null, $topics);
        return ($score);
    }
    ###End User Test Score###

    ###Classes Management###
    public function list_classes()
    {
        if ($this->admin_check() == false) {
            return false;
        }
        $classes = $this->sql_to_array("SELECT * FROM `classes` WHERE `admin`=" . $this->user_info->id . " ORDER BY `name` DESC");
        return $classes;
    }
    public function add_class(string $name)
    {
        //expecting mail/id of user and an array containing new information
        //checking wether id/mail
        if ($this->admin_check()) {
            //Preparing and escaping the values
            $name = $this->db->real_escape_string(ucwords($name));
            $school = $this->db->real_escape_string($this->user_info->school);
            $admin = $this->user_info->id;
            $sql = "INSERT INTO classes (`name`, `school`, `admin`) VALUES('$name', '$school', '$admin')";
            $this->db->query($sql) or die("Error in MySQL query.");
            return true;
        }
        return false;
    }
    public function update_class(int $class, array $update)
    {
        //expecting mail/id of user and an array containing new information
        //checking wether id/mail
        if ($this->admin_check()) {
            //Just allow access if admin of class
            if ($this->db->query("SELECT * FROM classes WHERE id=$class AND `admin`=" . $this->user_info->id)->num_rows != 1) {
                return false;
            }

            //Preparing and escaping the values
            if (isset($update["name"])) {
                $update["name"] = $this->db->real_escape_string(ucwords($update["name"]));
            }

            if (isset($update["topics"])) {
                $update["topics"] = $this->db->real_escape_string(json_encode($update["topics"]));
            }

            if (isset($update["students"])) {
                $update["students"] = $this->db->real_escape_string(json_encode($update["students"]));
            }

            $sql = "UPDATE classes SET ";
            foreach ($update as $key => $value) {
                $sql .= "$key='$value', ";
            }
            $sql = substr($sql, 0, -2);
            $sql .= " WHERE id=$class";
            $this->db->query($sql) or die("Error in MySQL query.");
            return true;
        }
        return false;
    }
    public function delete_class(int $id)
    {
        if ($this->admin_check()) {
            //Just allow access if admin of class
            if ($this->db->query("SELECT * FROM classes WHERE id=$id AND `admin`=" . $this->user_info->id)->num_rows != 1) {
                return false;
            }

            $sql = "DELETE FROM classes WHERE id='$id'";
            $this->db->query($sql) or die("Error in MySQL query.");
            return true;
        }
        return false;
    }
    public function class_list_join_tests(int $class)
    {
        if ($this->admin_check()) {
            //Just allow access if admin of class
            if ($this->db->query("SELECT * FROM classes WHERE id=$class AND `admin`=" . $this->user_info->id)->num_rows != 1) {
                return false;
            }

            //Computing names of all JoinTests first
            $join_test_names = [];
            $all_join_tests = $this->sql_to_array("SELECT * FROM `join`");
            foreach ($all_join_tests as $join_test) {
                $join_test_names[$join_test->id] = $join_test->name;
            }
            //Listing joinInstances for given class
            $sql = "SELECT * FROM `join_instances` WHERE `class`=$class";
            $join_tests = $this->sql_to_array($sql);
            if (count($join_tests) == 0) {
                return false;
            }

            $join_test_list = [];
            foreach ($join_tests as $test) {
                $join_score = [
                    "id" => $test->id,
                    "joinTest" => [
                        "id" => $test->join_test,
                        "name" => $join_test_names[$test->join_test],
                    ],
                    "date" => $test->expiry - $test->writing_time,
                ];
                $join_test_list[] = $join_score;
            }
            return $join_test_list;
        }
        return false;
    }
    public function leave_class(int $id)
    {
        if ($this->check_login() == false) {
            return (false);
        }

        $user = $this->user_info->id;
        $row = $this->db->query("SELECT * FROM classes WHERE id=$id")->fetch_object();
        $members = json_decode($row->students);
        if (in_array($user, $members)) {
            unset($members[array_search($user, $members)]);
            sort($members);
            $members = $this->db->real_escape_string(json_encode($members));
            $this->db->query("UPDATE classes SET students='$members' WHERE id=$id") or die("Error in MySQL query.");
            return (true);
        } else {
            return (false);
        }
    }
    public function in_classes($user = null)
    {
        if ($this->check_login() == false) {
            return (false);
        }
        //If needed, reading user #id
        if (is_null($user) || $user == $this->user_info->id) {
            $user = $this->user_info->id;
            $sql = "SELECT * FROM classes WHERE students RLIKE '(\"|,|^|\\[)$user(\"|,|$|\\])'";
        } else {
            if ($this->admin_check() == false) {
                return false;
            }

            $admin = $this->user_info->id;
            $sql = "SELECT * FROM classes WHERE `admin`=$admin AND students RLIKE '(\"|,|^|\\[)$user(\"|,|$|\\])'";
        }
        $result = $this->sql_to_array($sql);
        if (is_array($result) == false) {
            return (false);
        }

        foreach ($result as $current_class) {
            unset($current_class->school);
            unset($current_class->topics);
            unset($current_class->students);
        }
        return ($result);
    }
    public function class_list_assignments(int $class)
    {
        if ($this->admin_check()) {
            //Just allow access if admin of class
            if ($this->db->query("SELECT * FROM classes WHERE id=$class AND `admin`=" . $this->user_info->id)->num_rows != 1) {
                return false;
            }

            //Listing assignments for given class
            $sql = "SELECT * FROM `assignments` WHERE `class`=$class";
            $assignments = $this->sql_to_array($sql);
            if (count($assignments) == 0) {
                return [];
            }

            $assignments_list = [];
            foreach ($assignments as $assignment) {
                $assignments_list[] = $this->class_assignment_details($assignment->id);
            }
            return $assignments_list;
        }
        return false;
    }
    public function class_assignment_details(int $id)
    {
        //Checking for admin rights
        if ($this->admin_check() == false) {
            return false;
        }
        //Just allow access if admin of assignment
        if ($this->db->query("SELECT * FROM assignments WHERE id=$id AND `admin`=" . $this->user_info->id)->num_rows != 1) {
            return false;
        }

        //Assignment details
        $sql = "SELECT * FROM `assignments` WHERE `id`=$id";
        $assignment = $this->sql_to_array($sql)[0];

        $exercise_ids = json_decode($assignment->exercises);
        $exercises = $this->fetch_exercises($exercise_ids,null,null,true);
        if($exercises==false) $exercises = [];

        //Comparing test admin with current user
        /*$admin=$this->sql_to_array("SELECT `admin` FROM `join` WHERE id=$id")[0]->admin;
        if($admin!=$this->user_info->id) return false;*/
        //Fetching all tests with given join_id
        $tests = [];
        $total_score = 0;

        $sql = "SELECT * FROM tests WHERE assignment=$id";
        $fetched_tests = $this->sql_to_array($sql);
        if (is_array($fetched_tests)) {
            foreach ($fetched_tests as $test) {
                $test_score = $this->test_score($test->id, true);
                $score = $test_score["score"];
                // Computing score if exercises missing in student's responses
                if(count($test_score["answers"])!=count($exercises)) {
                    $score = ( $score / count($exercises) ) * count($test_score["answers"]);
                }
                $test_user = $this->get_user($test->user);
                $test_part = [
                    "id" => $test->id,
                    "score" => $score,
                    "completedScore" => $test_score["score"],
                    "completed" => count($test_score["answers"]) / count($exercises),
                    "user" => $test_user,
                    "late" => ($assignment->expiry >= $test->timestamp),
                    "timestamp" => ($assignment->expiry >= $test->timestamp) ? $test->timestamp : false
                ];
                $tests[] = $test_part;
                $total_score += $score;
            }
        }

        //Computing average test score
        $total_score /= count($fetched_tests);
        $assignment_details = [
            "id" => $assignment->id,
            "name" => $assignment->name,
            "describtion" => $assignment->describtion,
            "expiry" => $assignment->expiry,
            "exercises" => $exercises,
            "score" => $total_score,
            "tests" => $tests,
        ];
        return ($assignment_details);
    }
    ###End Classes Management###

    ###Vouchers###
    public function create_voucher()
    {
        if (!$this->admin_check()) {
            return (false);
        }

        $school = $this->user_info->school;
        $timestamp = time() + 7 * 24 * 3600;
        $code = $this->random_str(6);
        $this->db->query("INSERT INTO `vouchers` (`code`, `school`, `expiry`) VALUES ('$code', $school, $timestamp)");
        return ([
            "code" => $code,
            "expiry" => $timestamp,
        ]);
    }
    public function list_vouchers()
    {
        if (!$this->admin_check()) {
            return (false);
        }

        $school = $this->user_info->school;
        $timestamp = time();
        $vouchers = $this->sql_to_array("SELECT * FROM `vouchers` WHERE `school`=$school AND `expiry`>$timestamp");
        $vouchers = array_reverse($vouchers);
        return ($vouchers);
    }
    /**
     * Generate a random string, using a cryptographically secure
     * pseudorandom number generator (random_int)
     *
     * This function uses type hints now (PHP 7+ only), but it was originally
     * written for PHP 5 as well.
     *
     * For PHP 7, random_int is a PHP core function
     * For PHP 5.x, depends on https://github.com/paragonie/random_compat
     *
     * @param int $length      How many characters do we want?
     * @param string $keyspace A string of all possible characters
     *                         to select from
     * @return string
     */
    public function random_str(
        int $length = 64,
        string $keyspace = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    ): string {
        if ($length < 1) {
            throw new \RangeException("Length must be a positive integer");
        }
        $pieces = [];
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $pieces[] = $keyspace[random_int(0, $max)];
        }
        return implode('', $pieces);
    }

    public function topic_year_overwrite(int $topic, int $year) {
        if ($this->admin_check() == false || $year > 13) {
            return (false);
        }
        $this->db->query("DELETE FROM `topic_years` WHERE `topic_id` = $topic AND `school` = ".$this->user_info->school);
        $this->db->query("INSERT INTO `topic_years` (`school`, `year`, `topic_id`) VALUES (".$this->user_info->school.", $year, $topic)");
        return(true);
    }
    ###End Vouchers###

    ###Anything else###
    public function list_schools()
    {
        $result = $this->sql_to_array("SELECT * FROM schools ORDER BY `name` ASC");
        if (is_array($result) == false) {
            return (false);
        }

        return $result;
    }
    public function list_students(bool $students_only = true)
    {
        if ($this->admin_check() == false) {
            return (false);
        }

        $school = $this->user_info->school;
        $sql = "SELECT `id`,`name`,`mail` FROM `user` WHERE `school`=$school";
        if ($students_only) {
            $sql .= " AND `accesslevel`=0";
        }

        $sql .= " ORDER BY `name` ASC";
        $result = $this->sql_to_array($sql) or die("Error in MySQL query.");
        return ($result);
    }
    public function list_topics($id = null)
    {
        $sql = "SELECT DISTINCT * FROM topics";
        if ($id != null) {
            $sql .= " WHERE id=$id";
        }

        $topics = $this->sql_to_array($sql);
        if (is_array($topics) == false) {
            return (false);
        }

        $return = [];
        foreach ($topics as $topic) {
            $school = $this->user_info->school;
            $topic_id = $topic->id;
            $year = $topic->year;
            $sql = "SELECT * FROM `topic_years` WHERE `school`=$school AND `topic_id`=$topic_id";
            $topic_year_overwrite = $this->sql_to_array($sql);
            if(is_array($topic_year_overwrite) && count($topic_year_overwrite)>0) $year = $topic_year_overwrite[0]->year; 
            $return[]=[
                "id" => $topic_id,
                "name" => $topic->name,
                "year" => $year,
                "timestamp" => $topic->timestamp,
                "subject" => $topic->subject
            ];
        }

        return ($return);
    }
    private function exercise_ids_by_topic_ids($topics)
    {
        $exercises = $this->sql_to_array("SELECT * FROM exercises");
        $topics_tmp = $topics;
        $filtered_exercises = array_filter($exercises, function ($exercise) use ($topics) {
            //Checking whether any matches between the exercise's topics and the given topics
            $containing = false;
            foreach ($topics as $value) {
                if (in_array($value, json_decode($exercise->topic))) {
                    $containing = true;
                }

            }
            return $containing;
        });
        if (count($filtered_exercises) == 0) {
            return ([]);
        }

        return ($this->object_column($filtered_exercises, 'id'));
    }
    public function add_topic(string $name, string $subject, int $year = 0)
    {
        $name = $this->db->real_escape_string($name);
        $subject = $this->db->real_escape_string($subject);
        $time = time() + (2 * 7 * 24 * 60 * 60); //Time plus 2 weeks
        if($year>13) $year = 0;
        $this->db->query("INSERT INTO topics (`name`, `subject`, `timestamp`, `year`) VALUES('$name', '$subject', '$time', $year)");
        return (true);
    }
    public function clean_db()
    {
        //Deleting old sessions from DB
        $this->db->query("DELETE FROM `sessions` WHERE `timestamp`<" . time());
        //Deleting old, unused topics
        //Checkin every topic every two weeks
        $topic_ids = $this->sql_to_array("SELECT id FROM `topics` WHERE `timestamp`<" . time());
        if (is_array($topic_ids) == false) {
            return (false);
        }

        $topic_ids = $this->object_column($topic_ids, "id");
        //Fetching all exercises topics
        $result = $this->db->query("SELECT topic FROM `exercises`");
        $exe_ids = [];
        while ($row = $result->fetch_object()) {
            foreach (json_decode($row->topic) as $id) {
                $exe_ids[] = $id;
            }
        }
        array_unique($exe_ids);
        $delete = [];
        foreach ($topic_ids as $topic) {
            if (in_array($topic, $exe_ids) == false) {
                $delete[] = $topic;
            }
        }
        if (empty($delete) == false) {
            $this->db->query("DELETE FROM `topics` WHERE id=" . implode(" OR id=", $delete));
        }
        $this->db->query("UPDATE `topics` SET `timestamp`=" . (time() + 3600 * 24 * 5) . " WHERE `timestamp`<" . time());

        //Deleting old vouchers
        $this->db->query("DELETE * FROM `vouchers` WHERE `expiry`<"+time());
    }
    private function sql_to_array(string $sql)
    {
        $result = $this->db->query($sql);
        if ($result->num_rows == 0) {
            return false;
        }

        $return = [];
        while ($row = $result->fetch_object()) {
            $return[] = $row;
        }
        $result->close();
        return ($return);
    }
    private function object_column(array $objects, $column)
    {
        $columns = [];
        foreach ($objects as $object) {
            $columns[] = $object->$column;
        }
        return ($columns);
    }
    ###Mailing function###
    public function mailer(string $to, string $subject, string $message)
    {
        try {
            require "mail.inc.php";
            $mailer = mailer();
            $to = strtolower($to);
            $mail_user = $this->get_user($to);
            $mail_name = "User";
            if ($mail_user != false) {
                $to = $mail_user->name . " <" . $mail_user->mail . ">";
                $mail_name = $mail_user->name;
            }

            //Handling signature image
            $signature = "data:image/png;base64," . base64_encode(file_get_contents("assets/signature.png"));

            // Message
            $message = "
                <html>
                <head>
                <title>$subject</title>
                </head>
                <body>
                <article>
                <section>
                <p>Dear $mail_name,<br/>
                $message<br/>
                Thank you,<br/>
                <img style=\"width:8rem;height:auto;\" src=\"$signature\"></p>
                <section>
                <article>
                </body>
                </html>
                ";

            // Mail it
            $mailer->Subject = $subject;
            $mailer->addAddress($mail_user->mail, $mail_name);
            $mailer->Body = "$message";
            try {
                $mailer->send();
            } catch (Exception $e) {
                $GLOBALS["errors"][] = "Error sending mail.";
            }
            return true;
        } catch (Exception $e) {
            $GLOBALS["errors"][]=$e;
        }
    }
}
