<?php
//make PHP showing errors
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/
if (isset($_GET["install"])) {
    require "install.php";
    if ($del) {
        unlink("install.php");
    }

    die(true);
}
//Requiring classes
require_once "classes.inc.php";

function parseArguments()
{
    $rawInput = file_get_contents('php://input');
    if ($rawInput != "") {
        try {
            $arguments = json_decode($rawInput, true);
            return ($arguments);
        } catch (\Throwable $th) {
            // What a pitty... No REST request...
        }
    }
    if (isset($_GET["json"])) { // Seems to be a JSON encoded GET-request
        try {
            $arguments = json_decode($_GET["json"], true);
            return $arguments;
        } catch (\Throwable $th) {
            // Nor it seems to be valid JSON in $_GET parameter...
        }
    }
    return $_GET;
}

function determinateJob($arguments)
{
    // Saving first key in $_GET
    $get_key = key($_GET);
    if (array_key_exists("job", $arguments)) {
        //key "job" existing in $arguments. Taking it.
        $job = $arguments["job"];
    } else if ($get_key != "json" && $get_key != "sid") {
        //Taking the first key of $_GET
        $job = $get_key;
    } else {
        //Taking the first key in $arguments
        $job = key($arguments);
    }
    return $job;
}
//Reading arguments into variable
$arguments = parseArguments();
//Checking if need to restore session
if (isset($arguments["sid"])) {
    $testapp = new testapp($arguments["sid"]);
    $testapp->restore_session($arguments["sid"]);
} else {
    $testapp = new testapp();
}
//Reading the requested job form $arguments if not included directly in $_GET
$job = determinateJob($arguments);

//Collecting errors in Array
$errors = [];

//Case for every API command
switch ($job) {
    ###User Management###
    case 'login':
        if (!isset($arguments["mail"]) or !isset($arguments["pass"])) {$return = false;
            break;}
        if (isset($arguments["stay"])) {
            //Converting to bool
            $stay_logged_in = ($arguments["stay"] == "true" || $arguments["stay"] == true || $arguments["stay"] == 1) ? true : false;
        } else {
            $stay_logged_in = true;
        }
        $return = $testapp->login($arguments["mail"], $arguments["pass"], $stay_logged_in);
        break;

    case 'logout':
        $return = $testapp->logout();
        break;

    case 'loginState':
        $return = $testapp->check_login();
        break;

    case 'register':
        if (isset($arguments["mail"]) && isset($arguments["pass"]) && isset($arguments["school"]) && isset($arguments["name"])) {
            if (isset($arguments["accesslevel"])) {
                $return = $testapp->create_user_account($arguments["mail"], $arguments["pass"], $arguments["name"], $arguments["school"], $arguments["accesslevel"], (isset($arguments["voucher"]) ? $arguments["voucher"] : null));
                break;
            } else {
                $return = $testapp->create_user_account($arguments["mail"], $arguments["pass"], $arguments["name"], $arguments["school"], 0);
                break;
            }
        }
        $return = false;
        break;

    case 'updateUser':
        $update = [];
        if (isset($arguments["pass"])) {
            $update["pass"] = $arguments["pass"];
        }

        if (isset($arguments["mail"])) {
            $update["mail"] = $arguments["mail"];
        }

        if (isset($arguments["school"])) {
            $update["school"] = $arguments["school"];
        }

        if (isset($arguments["name"])) {
            $update["name"] = $arguments["name"];
        }

        if (isset($arguments["user"])) {
            $return = $testapp->update_user($arguments["user"], $update);
            break;
        } else {
            $return = $testapp->update_user(null, $update);
            break;
        }
        $return = false;
        break;

    case 'passwordReset':
        if (isset($arguments["mail"])) {
            $return = $testapp->password_reset($arguments["mail"]);
        }

        break;

    case 'userInfo':
        $user = (isset($arguments["user"])) ? $arguments["user"] : null;
        $return = $testapp->get_user($user);
        break;

    case 'flame':
        $return = $testapp->flame();
        break;
    ###Exercises###
    case 'addExercise':
        if (isset($arguments["name"]) && isset($arguments["content"]) && isset($arguments["correct"]) && isset($arguments["type"]) && isset($arguments["answers"]) && isset($arguments["topics"])) {
            $name = $arguments["name"];
            $content = $arguments["content"];
            $correct = $arguments["correct"];
            $type = $arguments["type"];
            $answers = $arguments["answers"];
            $topics = $arguments["topics"];
            $user = (isset($arguments["tags"])) ? $arguments["tags"] : null;
            $user = (isset($arguments["explanation"])) ? $arguments["explanation"] : null;
            $return = $testapp->add_exercise($name, $content, $topics, $correct, $type, $answers, $tags, $explanation);
        } else {
            $return = false;
        }
        break;

    case 'updateExercise':
        $update = [];
        if (isset($arguments["exercise"])) {
            $id = $arguments["exercise"];
        }

        if (isset($arguments["topics"])) {
            $update["topic"] = $arguments["topics"];
        }

        if (isset($arguments["name"])) {
            $update["name"] = $arguments["name"];
        }

        if (isset($arguments["content"])) {
            $update["content"] = $arguments["content"];
        }

        if (isset($arguments["correct"])) {
            $update["correct"] = $arguments["correct"];
        }

        if (isset($arguments["type"])) {
            $update["awtype"] = $arguments["type"];
        }

        if (isset($arguments["answers"])) {
            $update["answers"] = $arguments["answers"];
        }

        if (isset($id)) {
            $return = $testapp->update_exercise($id, $update);
            break;
        }
        $return = false;
        break;
    case 'uploadImage':
        if (isset($arguments["exercise"])) {
            $return = $testapp->upload_image($arguments["exercise"]);
        }

        break;
    case 'removeImage':
        if (isset($arguments["exercise"])) {
            $return = $testapp->remove_image($arguments["exercise"]);
        }

        break;
    case 'deleteExercise':
        if (isset($arguments["exercise"])) {
            $return = $testapp->delete_exercise($arguments["exercise"]);
            break;
        }
        $return = false;
        break;
    case 'exerciseActivation':
        if (isset($arguments["exercise"]) && isset($arguments["activation"])) {
            $return = $testapp->exercise_activation($arguments["exercise"], $arguments["activation"]);
            break;
        }
        $return = false;
        break;

    case 'fetchExercises':
        if (isset($arguments["exercise"])) {
            $return = $testapp->fetch_exercises($arguments["exercise"]);
            break;
        } else {
            $topics = (isset($arguments["topics"])) ? $arguments["topics"] : null;
            $number = (isset($arguments["number"])) ? $arguments["number"] : null;
            $query = (isset($arguments["query"])) ? $arguments["query"] : null;
            $show_img = (isset($arguments["images"])) ? $arguments["images"] : false;
            $return = $testapp->fetch_exercises(null, $topics, $number, $show_img, $query);
            break;
        }
        $return = false;
        break;
    ###Test Management###
    case 'joinTest':
        if (isset($arguments["id"])) {
            $return = $testapp->join_test($arguments["id"]);
            break;
        }
        $return = false;
        break;

    case 'addJoin':
        if (isset($arguments["exercises"]) && isset($arguments["name"])) {
            $return = $testapp->add_join_test($arguments["exercises"], $arguments["name"]);
            break;
        }
        $return = false;
        break;

    case 'deleteJoin':
        if (isset($arguments["id"])) {
            $return = $testapp->delete_join_test($arguments["id"]);
            break;
        }
        $return = false;
        break;

    case 'listJoin':
        $show_all = (isset($arguments["showAll"])) ? $arguments["showAll"] : false;
        $return = $testapp->list_join_tests($show_all);
        break;
    case 'updateJoin':
        $exercises = (isset($arguments["exercises"])) ? $arguments["exercises"] : null;
        $name = (isset($arguments["name"])) ? $arguments["name"] : null;
        if (isset($arguments["id"])) {
            $return = $testapp->update_join_test($arguments["id"], $exercises, $name);
            break;
        }
        $return = false;
        break;

    case 'classJoinTests':
        if (isset($arguments["class"])) {
            $return = $testapp->class_list_join_tests($arguments["class"]);
            break;
        }
        $return = false;
        break;

    case 'activateJoinTest':
        if (isset($arguments["id"]) && isset($arguments["expiry"]) && isset($arguments["time"]) && isset($arguments["class"])) {
            $return = $testapp->activate_join_test($arguments["id"], $arguments["time"], $arguments["expiry"], $arguments["class"]);
            break;
        }
        $return = false;
        break;
    
    case 'addAssignment':
        if (isset($arguments["id"]) && isset($arguments["expiry"]) && isset($arguments["name"]) && isset($arguments["describtion"]) && isset($arguments["class"])) {
            $return = $testapp->add_assignment($arguments["id"], $arguments["expiry"], $arguments["name"], $arguments["describtion"], $arguments["class"]);
            break;
        }
        $return = false;
        break;
    
    case 'classAssignments':
        if (isset($arguments["class"])) {
            $return = $testapp->class_list_assignments($arguments["class"]);
            break;
        }
        $return = false;
        break;
    
    case 'assignmentDetails':
        if (isset($arguments["id"])) {
            $return = $testapp->class_assignment_details($arguments["id"]);
            break;
        }
        $return = false;
        break;
    
    case 'userAssignments':
        $return = $testapp->user_list_assignments();
        break;
    ###Test Usage###
    case 'saveTest':
        if (isset($arguments["answers"])) {
            $answers = $arguments["answers"];
        } else {
            $return = false;
            break;
        }
        $user = (isset($arguments["user"])) ? $arguments["user"] : null;
        $privacy = (isset($arguments["privacy"])) ? $arguments["privacy"] : "anonymous";
        $join = (isset($arguments["join"])) ? $arguments["join"] : null;
        $assignment = (isset($arguments["assignment"])) ? $arguments["assignment"] : null;
        $return = $testapp->save_test($user, $answers, $privacy, $join, $assignment);
        break;

    case 'testScore':
        if (isset($arguments["id"])) {
            $return = $testapp->test_score($arguments["id"]);
            break;
        }
        $return = false;
        break;

    case 'listTests':
        $user = (isset($arguments["user"])) ? $arguments["user"] : null;
        $return = $testapp->list_tests($user);
        break;

    case 'userScore':
        $return = $testapp->user_score();
        break;

    case 'userSubjectScore':
        $return = (isset($arguments["subject"])) ? $testapp->user_subject_score($arguments["subject"]) : false;
        break;

    case 'topicScore':
        $user = (isset($arguments["user"])) ? $arguments["user"] : null;
        if (isset($arguments["topics"])) {
            $return = $testapp->topic_score($user, $arguments["topics"]);
            break;
        }
        $return = false;
        break;

    case 'classScore':
        $topics = (isset($arguments["topics"])) ? $arguments["topics"] : null;
        if (isset($arguments["class"])) {
            $return = $testapp->class_score($arguments["class"], $topics);
            break;
        }
        $return = false;
        break;

    case 'joinScore':
        if (isset($arguments["join"])) {
            $return = $testapp->join_score($arguments["join"]);
            break;
        }
        $return = false;
        break;
    ###Class Management###
    case 'addClass':
        if (isset($arguments["name"])) {
            $return = $testapp->add_class($arguments["name"]);
            break;
        }
        $return = false;
        break;

    case 'listClasses':
        $return = $testapp->list_classes();
        break;

    case 'deleteClass':
        if (isset($arguments["class"])) {
            $return = $testapp->delete_class($arguments["class"]);
            break;
        }
        $return = false;
        break;

    case 'updateClass':
        $update = [];
        if (isset($arguments["class"])) {
            $class = $arguments["class"];
        }

        if (isset($arguments["name"])) {
            $update["name"] = $arguments["name"];
        }

        if (isset($arguments["topics"])) {
            $update["topics"] = $arguments["topics"];
        }

        if (isset($arguments["students"])) {
            $update["students"] = $arguments["students"];
        }

        if (isset($arguments["class"])) {
            $return = $testapp->update_class($class, $update);
            break;
        }
        $return = false;
        break;

    case 'inClasses':
        $user = (isset($arguments["user"])) ? $arguments["user"] : null;
        $return = $testapp->in_classes($user);
        break;

    case 'leaveClass':
        if (isset($arguments["class"])) {
            $return = $testapp->leave_class($arguments["class"]);
            break;
        }
        $return = false;
        break;
    ###Vouchers###
    case 'createVoucher':
        $return = $testapp->create_voucher();
        break;

    case 'listVouchers':
        $return = $testapp->list_vouchers();
        break;
    ###General Stuff###
    case 'listTopics':
        $return = $testapp->list_topics();
        break;

    case 'addTopic':
        if (isset($arguments["name"]) == false || isset($arguments["subject"]) == false) {
            $return = false;
            break;
        }
        $year = (isset($arguments["year"])) ? $arguments["year"] : 0;
        $return = $testapp->add_topic($arguments["name"], $arguments["subject"], $year);
        break;

    case 'topicYearOverwrite':
        if (isset($arguments["topic"]) == false || isset($arguments["year"]) == false) {
            $return = false;
            break;
        }
        $return = $testapp->topic_year_overwrite($arguments["topic"], $arguments["year"]);
        break;

    case 'listSchools':
        $return = $testapp->list_schools();
        break;

    case 'listStudents':
        $students_only = (isset($arguments["studentsOnly"])) ? ($arguments["studentsOnly"] == "true" || $arguments["studentsOnly"] == true || $arguments["studentsOnly"] == 1) : true;
        $return = $testapp->list_students($students_only);
        break;
    ###Help command###
    case 'help':
        $return = "TestApp API Interface
                    No API refference wrote yet. Please visit https://gitlab.com/JasMich.de/ for furtor information.
                    Are bears actually able to growl?";
        break;

    ###If no command given###
    default:
        $errors[] = "NO_JOB";
        $return = null;
        break;
}
$return_object = array(
    "job" => $job,
    "sid" => $testapp->session_id,
    "arguments" => $arguments,
    "errors" => $errors,
    "response" => $return,
);
echo (json_encode($return_object));
